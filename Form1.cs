﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Threading;
using System.Windows.Forms;

// Important: include the opencvsharp library in your code
using OpenCvSharp;
using OpenCvSharp.Extensions;

namespace SelfiesDemo
{
    public partial class Form1 : Form
    {
        // Create class-level accesible variables
        VideoCapture capture;
        Mat frame;
        Bitmap image;
        private Thread camera;
        bool isCameraRunning = false;


        // Declare required methods
        private void CaptureCamera()
        {
            camera = new Thread(new ThreadStart(CaptureCameraCallback));
            camera.Start();
        }

        private void CaptureCameraCallback()
        {

            frame = new Mat();
            capture = new VideoCapture(0);
            //capture.Open(0);

            if (capture.IsOpened())
            {
                while (isCameraRunning)
                {

                    capture.Read(frame);
                    Cv2.Resize(frame, frame, new OpenCvSharp.Size(pictureBox1.Width, pictureBox1.Height));

                    image = MatToBitmap(frame);
                    if (pictureBox1.Image != null)
                    {
                        pictureBox1.Image.Dispose();
                    }
                    pictureBox1.Image = image;
                }
            }
        }

        private void ConbinedImages(Mat bigImg, Mat contract)
        {
            //貼り付ける画像を50%縮小
            //var contract = new Mat();
            //Cv2.Resize(smallImg, contract, new OpenCvSharp.Size(0.5 * smallImg.Width, 0.5 * smallImg.Height), 0, 0, InterpolationFlags.Lanczos4);

            //貼り付ける範囲を指定して貼り付け
            var rect = new Rect((bigImg.Width - contract.Width) / 2, 0, contract.Width, contract.Height);
            var pastedMat = new Mat();
            Cv2.AddWeighted(new Mat(bigImg, rect), 0.2, contract, 0.8, 0, pastedMat);
            //※AddWeightedを使わずにAddを使うとアルファブレンディングしない<s>ただの貼り付け</s>

            //貼り付けた部分を置き換え
            bigImg[rect] = pastedMat;

            Cv2.ImWrite("ConbinedImage.png", bigImg);

            contract.Dispose();
            pastedMat.Dispose();
            bigImg.Dispose();

        }
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            Dispose();
            System.Environment.Exit(0);
            //if (MessageBox.Show("是否確定要關閉程式", "關閉程式", MessageBoxButtons.YesNo) == DialogResult.Yes)
            //{
            //    //e.Cancel = true;
            //    Dispose();
            //    System.Environment.Exit(0);
            //}
        }
        // When the user clicks on the start/stop button, start or release the camera and setup flags
        private void start_Click(object sender, EventArgs e)
        {
            if (button1.Text.Equals("Start"))
            {
                isCameraRunning = true;
                CaptureCamera();
                button1.Text = "Stop";
            }
            else
            {
                isCameraRunning = false;
                capture.Release();
                button1.Text = "Start";
            }
        }
        // When the user clicks on take snapshot, the image that is displayed in the pictureBox will be saved in your computer
        private void save_Click(object sender, EventArgs e)
        {
            if (isCameraRunning)
            {
                Bitmap snapshotBack = new Bitmap(pictureBox1.Image);

                if (pictureBox2.Image != null)
                {
                    Bitmap snapshotFore = new Bitmap(pictureBox2.Image);
                    Graphics g = Graphics.FromImage(snapshotBack);// create graphics object from big image,
                    g.DrawImage(snapshotFore, new System.Drawing.Point(0, 0));//draw small image over big image starting from position 0,0

                }
                // Save in some directory
                // in this example, we'll generate a random filename e.g 47059681-95ed-4e95-9b50-320092a3d652.png
                // snapshot.Save(@"C:\Users\sdkca\Desktop\mysnapshot.png", ImageFormat.Png);
                if (!Directory.Exists("Photos"))
                    Directory.CreateDirectory("Photos");

                snapshotBack.Save(string.Format(@"Photos\{0}.png", Guid.NewGuid()), ImageFormat.Png);
            }
            else
            {
                Console.WriteLine("Cannot take picture if the camera isn't capturing image!");
            }
        }
        private void filter_Click(object sender, EventArgs e)
        {
            // load filter image
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Title = "Select file";
            dialog.InitialDirectory = ".\\";
            dialog.Filter = "Image files (*.jpg, *.jpeg, *.jpe, *.jfif, *.png) | *.jpg; *.jpeg; *.jpe; *.jfif; *.png";
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                //MessageBox.Show(dialog.FileName);
                var img = Image.FromFile(dialog.FileName);
                var imgMat = BitmapToMat(new Bitmap(img));
                Cv2.Resize(imgMat, imgMat, new OpenCvSharp.Size(pictureBox1.Width, pictureBox1.Height));

                //var imageSize = pictureBox1.Image.Size;
                //var fitSize = pictureBox2.ClientSize;
                //pictureBox2.SizeMode = imageSize.Width > fitSize.Width || imageSize.Height > fitSize.Height ?
                //    PictureBoxSizeMode.Zoom : PictureBoxSizeMode.CenterImage;

                pictureBox2.Visible = true;
                pictureBox1.Controls.Add(pictureBox2);
                pictureBox2.BringToFront();
                pictureBox2.Location = new System.Drawing.Point(0, 0);
                //pictureBox2.SetBounds(0, 0, pictureBox1.Width, pictureBox1.Height);
                pictureBox2.BackColor = Color.Transparent;
                pictureBox2.Parent = pictureBox1;
                pictureBox2.Image = MatToBitmap(imgMat);
            }

            //var filter = new Mat(dialog.FileName);
            //Console.WriteLine(filter.Size());
            ////Cv2.Resize(smallImg, contract, new OpenCvSharp.Size(0.5 * smallImg.Width, 0.5 * smallImg.Height), 0, 0, InterpolationFlags.Lanczos4);
            ////Cv2.Resize(filter, filter, new OpenCvSharp.Size(321, 259), 0, 0, InterpolationFlags.Lanczos4);
            //Console.WriteLine(filter.Size());
            //pictureBox2.Image = BitmapConverter.ToBitmap(filter);
            //pictureBox1.Image = BitmapConverter.ToBitmap(filter);


        }

        private void PictureBox2_Resize(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        public Mat BitmapToMat(Bitmap image)
        {
            return OpenCvSharp.Extensions.BitmapConverter.ToMat(image);
        }
        public Bitmap MatToBitmap(Mat image)
        {
            return OpenCvSharp.Extensions.BitmapConverter.ToBitmap(image);
        }

    }
}